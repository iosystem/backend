﻿using BackendTransaccionSystemGCS.DAL.Repositories;
using BackendTransaccionSystemGCS.DAL.UnitOfWorks.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTransaccionSystemGCS.DAL.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DB _contest;
        public BankRepository BackRepository { get; set; }
        public ClientRepository ClientRepository { get; set; }
        public TransactionRepository TransactionRepository { get; set; }
        public UnitOfWork(DB contest)
        {
            _contest = contest;
            BackRepository = new BankRepository(_contest);
            ClientRepository = new ClientRepository(_contest);
            TransactionRepository = new TransactionRepository(_contest);
        }
        public DB Context(){ return _contest; }
        public int Complete()
        {
            return _contest.SaveChanges() ;
        }
    }
}
