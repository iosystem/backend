﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTransaccionSystemGCS.DAL.UnitOfWorks.Interfaces
{
    public interface IUnitOfWork
    {
        int Complete();
    }
}
