﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTransaccionSystemGCS.DAL.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public string IdTransaction { get; set; }
        public int IdFrontClient { get; set; }
        public int IdToClient { get; set; }
        public decimal Amount { get; set; }
        public Int16 TypeTransaccion { get; set; }
    }
}
