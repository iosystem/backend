﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTransaccionSystemGCS.DAL.Models
{
    public class Bank
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Client> Clients { get; set; }
    }
}
