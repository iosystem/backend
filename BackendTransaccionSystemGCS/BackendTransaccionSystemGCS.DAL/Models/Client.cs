﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTransaccionSystemGCS.DAL.Models
{
    public class Client
    {
        public int Id { get; set; }
        public int IdAcount { get; set; }
        public decimal Balance { get; set; }

        public virtual Bank Bank { get; set; }
    }
}
