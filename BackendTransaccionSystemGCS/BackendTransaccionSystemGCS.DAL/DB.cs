﻿using BackendTransaccionSystemGCS.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace BackendTransaccionSystemGCS.DAL
{
    public class DB:DbContext
    {
        public DB() {
        }
        public DB(DbContextOptions<DB> options) : base(options)
        {
            
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("Server=pruebagsc.mysql.database.azure.com; Port=3306; Database=transaccionGCS; Uid=test@pruebagsc; Pwd=Falcon07.; SslMode=Preferred;");
        }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Transaction> Transaccions { get; set; }

    }
}
