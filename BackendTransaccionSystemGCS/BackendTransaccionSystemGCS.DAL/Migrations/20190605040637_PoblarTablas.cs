﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendTransaccionSystemGCS.DAL.Migrations
{
    public partial class PoblarTablas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData("banks", "Name", "Popular");
            migrationBuilder.InsertData("clients", new string[] {"IdAcount","BankId","Balance" },new object[] {100,1,1000 });
            migrationBuilder.InsertData("clients", new string[] { "IdAcount", "BankId", "Balance" }, new object[] { 102, 1, 1000 });
            migrationBuilder.InsertData("clients", new string[] { "IdAcount", "BankId", "Balance" }, new object[] { 103, 1, 1000 });
            migrationBuilder.InsertData("clients", new string[] { "IdAcount", "BankId", "Balance" }, new object[] { 104, 1, 1000 });
            migrationBuilder.InsertData("clients", new string[] { "IdAcount", "BankId", "Balance" }, new object[] { 105, 1, 1000 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
