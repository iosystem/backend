﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendTransaccionSystemGCS.DAL.Migrations
{
    public partial class CambioTransacciones : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IdTransaction",
                table: "Transaccions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdTransaction",
                table: "Transaccions");
        }
    }
}
