﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendTransaccionSystemGCS.DAL.Migrations
{
    public partial class seEliminaColumnaCliente : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdBank",
                table: "Clients");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdBank",
                table: "Clients",
                nullable: false,
                defaultValue: 0);
        }
    }
}
