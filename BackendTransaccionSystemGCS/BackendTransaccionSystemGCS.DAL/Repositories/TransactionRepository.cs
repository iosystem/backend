﻿using BackendTransaccionSystemGCS.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTransaccionSystemGCS.DAL.Repositories
{
    public class TransactionRepository: Repository<Transaction>
    {
        public TransactionRepository(DB context) : base(context)
        {

        }
    }
}
