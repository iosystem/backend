﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace BackendTransaccionSystemGCS.DAL.Repositories.Interfaces
{
    public interface IRepository<TEntity> where TEntity:class
    {
        TEntity ByID(int id);
        IEnumerable<TEntity> All();
        IEnumerable<TEntity> FindAll(Expression <Func<TEntity,bool>> tocken);
        void Add(TEntity entity);
    }
}
