﻿
using BackendTransaccionSystemGCS.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTransaccionSystemGCS.DAL.Repositories
{
    public class ClientRepository:Repository<Client>
    {
        public ClientRepository(DB context):base(context)
        {

        }
    }
}
