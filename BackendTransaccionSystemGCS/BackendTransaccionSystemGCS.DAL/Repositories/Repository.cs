﻿using BackendTransaccionSystemGCS.DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Text;

namespace BackendTransaccionSystemGCS.DAL.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DB _contest;

        public Repository(DB contest)
        {
            _contest = contest;
        }
        public void Add(TEntity entity)
        {
            _contest.Add(entity);
        }
        public TEntity ByID(int id)
        {
            return _contest.Set<TEntity>().Find(id);
        }
        public IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> token)
        {
            return _contest.Set<TEntity>().Where(token);
        }
        public IEnumerable<TEntity> All()
        {
            return _contest.Set<TEntity>().ToList();
        }
    }
}
