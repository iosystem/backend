﻿using BackendTransaccionSystemGCS.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTransaccionSystemGCS.DAL.Repositories
{
    public class BankRepository : Repository<Bank>
    {
        public BankRepository(DB context) : base(context)
        {

        }
    }
}
