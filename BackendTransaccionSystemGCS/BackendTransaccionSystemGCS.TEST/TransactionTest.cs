using BackendTransaccionSystemGCS.BAL.Business;
using BackendTransaccionSystemGCS.BAL;
using NUnit.Framework;
using BackendTransaccionSystemGCS.DAL;
using Microsoft.EntityFrameworkCore;
using BackendTransaccionSystemGCS.DAL.Models;
using System.Linq;

namespace Tests
{
    [TestFixture]
    public class TransactionTest
    {
        [Test]
        public void CreateTransaction_DoTransaction_DevitsAndCreditsBothClients()
        {
            //Arrange
            var context = new DB();
            // Creates the database if not exists
            context.Database.EnsureCreated();
            var TransactionBS = new TransactionBS(context);
            //Act
            TransactionBS.CreateTransaction(100, 105, 50.50m);
            //Assert
            var reverseTransaction = context.Set<Transaction>()
                               .Where(p => p.IdFrontClient == 105 &&
                                      p.IdToClient == 100 &&
                                      p.TypeTransaccion ==  2).FirstOrDefault();
      
            Assert.That(reverseTransaction,!Is.Null);
        }
    }
}