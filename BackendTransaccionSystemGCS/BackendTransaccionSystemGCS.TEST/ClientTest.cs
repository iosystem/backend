﻿using BackendTransaccionSystemGCS.BAL.Business;
using BackendTransaccionSystemGCS.BAL;
using NUnit.Framework;
using BackendTransaccionSystemGCS.DAL;
using Microsoft.EntityFrameworkCore;
using BackendTransaccionSystemGCS.DAL.Models;
using System.Linq;

namespace Tests
{
    [TestFixture]
    public class ClientTest
    {
        [Test]
        public void CreateTransaction_DoTransaction_InsufficientFunds()
        {
            //Arrange
            var context = new DB();
            // Creates the database if not exists
            context.Database.EnsureCreated();
            var clientBS = new ClientBS(context);
            
            //Act
            var result = clientBS.CreateTransaction(100, 104, 2000);
            //Assert
            var message = new Message
            {
                Code = 6,
                Description = "Balance insuficiente."
            };
            //Assert.a(result, message);
            Assert.AreEqual(result.Code, message.Code);
            Assert.AreEqual(result.Description, message.Description);

        }
        [Test]
        public void CreateTransaction_DoTransaction_AmountLimit()
        {
            //Arrange
            var context = new DB();
            // Creates the database if not exists
            context.Database.EnsureCreated();
            var clientBS = new ClientBS(context);

            //Act
            var result = clientBS.CreateTransaction(100, 104, 500000);
            //Assert
            var message = new Message
            {
                Code = 5,
                Description = "Monto limite de transaccion superada."
            };
            //Assert.a(result, message);
            Assert.AreEqual(result.Code, message.Code);
            Assert.AreEqual(result.Description, message.Description);
        }
        [Test]
        public void CreateTransaction_DoTransaction_InvalidClient()
        {
            //Arrange
            var context = new DB();
            // Creates the database if not exists
            context.Database.EnsureCreated();
            var clientBS = new ClientBS(context);

            //Act
            var result = clientBS.CreateTransaction(100, 80, 50);
            //Assert
            var message = new Message
            {
                Code = 4,
                Description = "La cuenta del cliente de destino no existe."
            };
            //Assert.a(result, message);
            Assert.AreEqual(result.Code, message.Code);
            Assert.AreEqual(result.Description, message.Description);
        }
    }
}
