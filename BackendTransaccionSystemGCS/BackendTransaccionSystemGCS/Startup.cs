﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackendTransaccionSystemGCS.BAL.Business;
using BackendTransaccionSystemGCS.BAL.Interfaces;
using BackendTransaccionSystemGCS.DAL;
using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace BackendTransaccionSystemGCS
{
    public class Startup
    {
        private readonly ILogger _logger;

        public Startup(IConfiguration configuration , ILogger<Startup> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //This line adds Swagger generation services to our container.
            services.AddSwaggerGen(c =>
            {
                //The generated Swagger JSON file will have these properties.
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Transaccion API.",
                    Version = "v1",
                    Description = "Example API created to handle bank transactions between bank customers.",
                    TermsOfService = "None",
                    Contact = new Contact { Name = "Jonatan Delgado Valdez", Email = "jontandelgadovaldez@gmail.com" }
                });

            });
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                    });
            });
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAll"));
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            
            services.AddScoped<IClientBS,ClientBS>();
            services.AddScoped<ITransactionBS, TransactionBS>();

            services.AddDbContext<DB>(options =>
                options.UseMySQL(Configuration["ConnectionString:ProductionConnection"]));

            // The following line enables Application Insights telemetry collection.
            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddSingleton<ITelemetryInitializer>(new ServiceNameInitializer());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSwagger();

            //This line enables Swagger UI, which provides us with a nice, simple UI with which we can view our API calls.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", " Transaccion API");
                c.RoutePrefix = "help";
            });
            app.UseCors("AllowAll");
            app.UseHttpsRedirection();
            app.UseMvc();
        }

        internal class ServiceNameInitializer : ITelemetryInitializer
        {
            public void Initialize(ITelemetry telemetry)
            {
                telemetry.Context.Cloud.RoleName = "BackendTransaccionSystemGCS";
            }
        }
    }
}
