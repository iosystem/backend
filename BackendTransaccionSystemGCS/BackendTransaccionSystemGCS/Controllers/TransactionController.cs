﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackendTransaccionSystemGCS.BAL;
using BackendTransaccionSystemGCS.BAL.Interfaces;
using BackendTransaccionSystemGCS.DAL;
using BackendTransaccionSystemGCS.DAL.Models;
using BackendTransaccionSystemGCS.DTO;
using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace BackendTransaccionSystemGCS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IClientBS _clientBS;
        private readonly TelemetryClient _insightsClient;

        public TransactionController(IConfiguration configuration, ILogger<Startup> logger, IClientBS clientBS, TelemetryClient insightsClient)
        {
            _configuration = configuration;
            _logger = logger;
            _clientBS = clientBS;
            _insightsClient = insightsClient;
        }

        // GET api/TransactionController
        [HttpGet("ConsultBalance")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(ClienteDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Message), StatusCodes.Status400BadRequest)]
        public ActionResult ConsultBalance([FromQuery]int idAccount)
        {
            try
            {
                var result = new ClienteDTO
                {
                    Account = idAccount,
                    Balance = _clientBS.GetBalance(idAccount)
                };
                _insightsClient.TrackTrace("Entrega Balance de la cuenta "+ idAccount);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _insightsClient.TrackException(ex);
                return BadRequest(new Message { Code = 1,Description = ex.Message });
            }
        }
        // POST api/TransactionController
        [HttpPost("MakeTransaction")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(Message),StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Message), StatusCodes.Status400BadRequest)]
        public ActionResult MakeTransaction([FromBody] TransactionDTO transaction)
        {
            try
            {
                var message = _clientBS.CreateTransaction(transaction.FromAccount, transaction.ToAccount, transaction.Amount);
                if (message.Code != 1)
                {
                    _insightsClient.TrackTrace("Se produjo un error manejado bajo el codigo: " + message.Code+
                        ", el mensaje del error es el siguiente: "+message.Description);
                    return BadRequest(message);
                }
                _insightsClient.TrackTrace("Transaccion realizada por el cliente " + transaction.FromAccount
                    +" y su mensaje de confirmacion fue el siguiente: ("+message.Description+")");
                return Ok(message);
            }
            catch (Exception ex)
            {
                _insightsClient.TrackException(ex);
                return BadRequest(new Message { Code = 0, Description = ex.Message });
            }
        }
    }
}
