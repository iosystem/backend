﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendTransaccionSystemGCS.DTO
{
    public class TransactionDTO
    {
        public decimal Amount { get; set; }
        public int FromAccount { get; set; }
        public int ToAccount { get; set; }
    }
}
