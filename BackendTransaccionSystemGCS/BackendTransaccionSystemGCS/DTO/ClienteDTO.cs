﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendTransaccionSystemGCS.DTO
{
    public class ClienteDTO
    {
        public int Account { get; set; }
        public decimal Balance { get; set; }
    }
}
