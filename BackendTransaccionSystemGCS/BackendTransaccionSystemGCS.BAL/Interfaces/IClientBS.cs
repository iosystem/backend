﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTransaccionSystemGCS.BAL.Interfaces
{
    public interface IClientBS
    {
        decimal GetBalance(int idAcount);
        bool ExistCliente(int idAcount);
        bool AllowedAmount(decimal amount);
        Message CreateTransaction(int idFrontClient, int IdToClient, decimal Amount);
    }
}
