﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTransaccionSystemGCS.BAL.Interfaces
{
    public interface ITransactionBS
    {
        string CreateTransaction(int idFrontClient, int IdToClient, decimal Amount);
    }
}
