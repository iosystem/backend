﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTransaccionSystemGCS.BAL
{
    public class Message
    {
        public int Code { get; set; }
        public string Description { get; set; }
    }
}
