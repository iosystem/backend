﻿using BackendTransaccionSystemGCS.BAL.Enum;
using BackendTransaccionSystemGCS.BAL.Interfaces;
using BackendTransaccionSystemGCS.DAL;
using BackendTransaccionSystemGCS.DAL.Models;
using BackendTransaccionSystemGCS.DAL.UnitOfWorks;
using System;
using System.Linq;

namespace BackendTransaccionSystemGCS.BAL.Business
{
    public class ClientBS: IClientBS
    {
        private UnitOfWork _unitOfWork;

        public ClientBS(DB db)
        {
            _unitOfWork = new UnitOfWork(db);
        }
        public decimal GetBalance(int idAcount)
        {
            try
            {
                var client = _unitOfWork.ClientRepository.FindAll(p => p.IdAcount == idAcount).FirstOrDefault();
                if (client == null)
                    throw new Exception("El cliente al que se busca no existe, no se puede regresar balance.");
                return client.Balance;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool ExistCliente(int idAcount)
        {
            try {

                var client = _unitOfWork.ClientRepository.FindAll(p => p.IdAcount == idAcount).FirstOrDefault();
                if (client == null)
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AllowedAmount(decimal amount)
        {
            if (amount > 20000)
                return false;
            return true;
        }
        public Message CreateTransaction(int idFrontClient, int IdToClient, decimal Amount) {
            try {
                if (idFrontClient == IdToClient)
                    return new Message { Code = 2, Description = "La cuenta de origen no puede ser igual a la cuenta destino" };
                if (!ExistCliente(idFrontClient))
                    return new Message { Code = 3, Description = "La cuenta del cliente de origen no existe." };
                if (!ExistCliente(IdToClient))
                    return new Message { Code = 4, Description = "La cuenta del cliente de destino no existe." };
                if (!AllowedAmount(Amount))
                    return new Message { Code = 5, Description = "Monto limite de transaccion superada." };
                if (GetBalance(idFrontClient) < Amount)
                    return new Message { Code = 6, Description = "Balance insuficiente." };

                var transaction = new TransactionBS(_unitOfWork.Context());
                var idTransaction = transaction.CreateTransaction(idFrontClient, IdToClient, Amount);
                return new Message { Code = 1, Description = "Transaccion realizada satisfactoriamente # de transaccion: "+ idTransaction };
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
