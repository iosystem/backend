﻿using BackendTransaccionSystemGCS.BAL.Enum;
using BackendTransaccionSystemGCS.BAL.Interfaces;
using BackendTransaccionSystemGCS.DAL;
using BackendTransaccionSystemGCS.DAL.Models;
using BackendTransaccionSystemGCS.DAL.UnitOfWorks;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace BackendTransaccionSystemGCS.BAL.Business
{
    public class TransactionBS: ITransactionBS
    {
        private UnitOfWork _unitOfWork;
        public TransactionBS(DB db)
        {
            _unitOfWork = new UnitOfWork(db);
        }
        public string CreateTransaction(int idFrontClient, int idToClient,decimal amount)
        {
            try
            {
                var idTransaction =  Guid.NewGuid().ToString();
                var fromClient = _unitOfWork.ClientRepository.FindAll(p => p.IdAcount == idFrontClient).FirstOrDefault();
                var toClient = _unitOfWork.ClientRepository.FindAll(p => p.IdAcount == idToClient).FirstOrDefault();
                var transaccionDebit = new Transaction
                {
                    IdFrontClient = idFrontClient,
                    IdToClient = idToClient,
                    Amount = amount,
                    TypeTransaccion = (int)TransactionType.Debito,
                    IdTransaction = idTransaction

                };
                
                var transaccionCredit = new Transaction
                {
                    IdFrontClient = idToClient,
                    IdToClient = idFrontClient,
                    Amount = amount,
                    TypeTransaccion = (int)TransactionType.Credito,
                    IdTransaction = Guid.NewGuid().ToString()

                };
                fromClient.Balance -= amount;
                toClient.Balance += amount;
                _unitOfWork.TransactionRepository.Add(transaccionDebit);
                _unitOfWork.TransactionRepository.Add(transaccionCredit);
                _unitOfWork.Complete();

                return transaccionDebit.IdTransaction;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
